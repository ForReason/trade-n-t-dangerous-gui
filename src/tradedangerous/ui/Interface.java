/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tradedangerous.ui;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import static java.lang.System.out;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author BechtJu
 */
public class Interface extends javax.swing.JFrame {
    Config conf = new Config();
    Commandbuilder command = new Commandbuilder();
    

    /**
     * Creates new form Interface
     * @throws java.io.IOException
     */
    public Interface() throws IOException{
        ChoosePath choosepath = new ChoosePath();
        
        //#####if no path is chosen - inicialize it!####//
        File file = new File("overall"); //inicialize standard config
        if(file.exists()){//if standardconfig exists check wether variable "path" exists. if not, ask user
            if(conf.getsinglevalue("overall", "path")==null){
                conf.setsingleConf("overall", "path", choosepath.Filechoose());
            }
        }
        else{
                conf.setsingleConf("overall", "path", choosepath.Filechoose());
        }

        initComponents();//inicialize ev.
        //#########if key set, update data#####//
        if (conf.getsinglevalue("overall", "update_on_startup") == null){
            conf.setsingleConf("overall", "update_on_startup", "false");
        }
        else if (conf.getsinglevalue("overall", "update_on_startup").equals("true")){
            try {
                command.singlecommand("trade.py import -P maddavo -O csvs");
            } catch (InterruptedException ex) {
                Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
            }
            updatedataonstartupcheckbox.setSelected(true);
        }
        
        //## inicialize loadshipcombobox##//
        file = new File("shipsettings");
        if (file.exists()){
        String[] props;
        String[] getlastsetting = {"shipsetting",};
        props = conf.getAllvalues("shipsettings");
        for (String prop : props) {
            loadshipcombobox.addItem(prop.substring(12));//substring for cutting the "shipsetting_SETTINGNAME"
            shipsettingscombobox.addItem(prop.substring(12)); //loadshipcombobox.addItem(item);
        }
        loadshipcombobox.setSelectedItem(conf.getConf("overall", getlastsetting)[0]); //set the last used settings
            try {
                loadshipbtnActionPerformed(null);//load the last used shipsettings
            } catch (Exception e) {
            }
        }
        
        file = new File("generalsettings");
        if (file.exists()){
        String[] props;
        String[] getlastsetting = {"generalsetting",};
        props = conf.getAllvalues("generalsettings");
        for (String prop : props) {
            loadgeneralcombobox.addItem(prop.substring(15));//substring for cutting the "shipsetting_SETTINGNAME"
            generalsettingscombobox.addItem(prop.substring(15)); //loadshipcombobox.addItem(item);
        }
        loadgeneralcombobox.setSelectedItem(conf.getConf("overall", getlastsetting)[0]); //set the last used settings
        try {
            loadgeneralbtnActionPerformed(null);//load the last used shipsettings
        }catch (Exception e) {
            }
        }
        file = new File("securitysettings");
        if (file.exists()){
        String[] props;
        String[] getlastsetting = {"securitysetting",};
        props = conf.getAllvalues("securitysettings");
        for (String prop : props) {
            loadsecuritycombobox.addItem(prop.substring(16));//substring for cutting the "shipsetting_SETTINGNAME"
            securitysettingscombobox.addItem(prop.substring(16)); //loadshipcombobox.addItem(item);
        }
        loadsecuritycombobox.setSelectedItem(conf.getConf("overall", getlastsetting)[0]); //set the last used settings
        try {
            loadsecuritybtnActionPerformed(null);//load the last used shipsettings
        }catch (Exception e) {
            }
        }
        file = new File("runsettings");
        if (file.exists()){
        String[] props;
        String[] getlastsetting = {"runsetting",};
        props = conf.getAllvalues("runsettings");
        for (String prop : props) {
            loadrunsettingcombobox.addItem(prop.substring(11));//substring for cutting the "shipsetting_SETTINGNAME"
        }
        loadrunsettingcombobox.setSelectedItem(conf.getConf("overall", getlastsetting)[0]); //set the last used settings
        try {
            loadbuttonActionPerformed(null);//load the last used shipsettings
        }catch (Exception e) {
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem4 = new javax.swing.JMenuItem();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelrun = new javax.swing.JPanel();
        consolescrollpane = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        manualcommandtxtfield = new javax.swing.JTextField();
        runcommandbutton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        modecombobox = new javax.swing.JComboBox();
        mode = new javax.swing.JLabel();
        startpoint = new javax.swing.JLabel();
        fromtxtfield = new javax.swing.JTextField();
        desttxtfield = new javax.swing.JTextField();
        destination = new javax.swing.JLabel();
        shipsettingscombobox = new javax.swing.JComboBox();
        shipsettings = new javax.swing.JLabel();
        generalsettingscombobox = new javax.swing.JComboBox();
        stationcountspinner = new javax.swing.JSpinner();
        generalsettings = new javax.swing.JLabel();
        securitysettings = new javax.swing.JLabel();
        Stationcount = new javax.swing.JLabel();
        Credits = new javax.swing.JLabel();
        creditsspinner = new javax.swing.JSpinner();
        securitysettingscombobox = new javax.swing.JComboBox();
        overridepresetpanel = new javax.swing.JPanel();
        insurance = new javax.swing.JLabel();
        uniquecheckbox = new javax.swing.JCheckBox();
        shortencheckbox = new javax.swing.JCheckBox();
        loopcheckbox = new javax.swing.JCheckBox();
        towardscheckbox = new javax.swing.JCheckBox();
        tradingrouteplanningcheckbox = new javax.swing.JCheckBox();
        updatepricescheckbox = new javax.swing.JCheckBox();
        insurancespinner = new javax.swing.JSpinner();
        avoidtext = new javax.swing.JLabel();
        viaoverridetxtfield = new javax.swing.JTextField();
        avoidoverridetxtfield = new javax.swing.JTextField();
        viatext = new javax.swing.JLabel();
        saverunsettingtxtfield = new javax.swing.JTextField();
        savebutton = new javax.swing.JButton();
        loadrunsettingcombobox = new javax.swing.JComboBox();
        loadbutton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        startrunbutton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        customcommandtxtfield = new javax.swing.JTextField();
        localpriceupdate = new javax.swing.JButton();
        jPanelsettings = new javax.swing.JPanel();
        Securitysettings = new javax.swing.JPanel();
        safeandloadsecuritypanel = new javax.swing.JPanel();
        loadsecuritybtn = new javax.swing.JButton();
        savesecuritybtn = new javax.swing.JButton();
        savesecurityfilename = new javax.swing.JTextField();
        loadsecuritycombobox = new javax.swing.JComboBox();
        securitydeletebutton = new javax.swing.JButton();
        moneysafetypanel = new javax.swing.JPanel();
        Insurance = new javax.swing.JLabel();
        Percentualinsurance = new javax.swing.JLabel();
        percentualinsurancetxtfield = new javax.swing.JTextField();
        percen1 = new javax.swing.JLabel();
        Margin = new javax.swing.JLabel();
        margintxtfield = new javax.swing.JTextField();
        insurancesecuritytxtfield = new javax.swing.JTextField();
        errorpreventionpanel = new javax.swing.JPanel();
        MinimumStock = new javax.swing.JLabel();
        minstocktextfield = new javax.swing.JTextField();
        mindemand = new javax.swing.JLabel();
        mindemandtxtfield = new javax.swing.JTextField();
        mincrperton = new javax.swing.JLabel();
        mincrpertontxtfield = new javax.swing.JTextField();
        maxcrperton = new javax.swing.JLabel();
        maxcrpertontxtfield = new javax.swing.JTextField();
        datasecuritypanel = new javax.swing.JPanel();
        maxdataage = new javax.swing.JLabel();
        maxdataagetxtfield = new javax.swing.JTextField();
        days = new javax.swing.JLabel();
        avoidviapanel = new javax.swing.JPanel();
        avoid = new javax.swing.JLabel();
        via = new javax.swing.JLabel();
        avoidtxtfield = new javax.swing.JTextField();
        viatxtfield = new javax.swing.JTextField();
        Shipsettings = new javax.swing.JPanel();
        saveandloadshippanel1 = new javax.swing.JPanel();
        loadshipbtn = new javax.swing.JButton();
        saveshipbtn = new javax.swing.JButton();
        saveshipfilename = new javax.swing.JTextField();
        loadshipcombobox = new javax.swing.JComboBox();
        shipdeletebutton = new javax.swing.JButton();
        jumpdistancepanel = new javax.swing.JPanel();
        unladen = new javax.swing.JLabel();
        unladentxtfield = new javax.swing.JTextField();
        laden = new javax.swing.JLabel();
        ladentxtfield = new javax.swing.JTextField();
        cargoloadpanel = new javax.swing.JPanel();
        capacity = new javax.swing.JLabel();
        capacitytxtfield = new javax.swing.JTextField();
        shipinsurancepanel = new javax.swing.JPanel();
        shipinsurance = new javax.swing.JLabel();
        shipinsurancetxtfield = new javax.swing.JTextField();
        shipsizepanel = new javax.swing.JPanel();
        shipsizeComboBox = new javax.swing.JComboBox();
        Generalsettigns = new javax.swing.JPanel();
        safeandloadgeneralpanel = new javax.swing.JPanel();
        loadgeneralbtn = new javax.swing.JButton();
        savegeneralbtn = new javax.swing.JButton();
        savegeneralfilename = new javax.swing.JTextField();
        loadgeneralcombobox = new javax.swing.JComboBox();
        generaldeletebutton = new javax.swing.JButton();
        vebositypanel = new javax.swing.JPanel();
        vebosity = new javax.swing.JLabel();
        vebositybox = new javax.swing.JComboBox();
        debuglevel = new javax.swing.JLabel();
        debuglevelbox = new javax.swing.JComboBox();
        routedistancepanelpanel = new javax.swing.JPanel();
        prunehops = new javax.swing.JLabel();
        prunescore = new javax.swing.JLabel();
        prunehopstxtfield = new javax.swing.JTextField();
        prunescoretxtfield = new javax.swing.JTextField();
        LSPenalty = new javax.swing.JLabel();
        MaxLS = new javax.swing.JLabel();
        lspentxtfield = new javax.swing.JTextField();
        maxlstxtfield = new javax.swing.JTextField();
        minhopsbeforeloop = new javax.swing.JLabel();
        minhopsbeforelooptxtfield = new javax.swing.JTextField();
        jumps = new javax.swing.JLabel();
        jumpstxtfield = new javax.swing.JTextField();
        miscpanel = new javax.swing.JPanel();
        uniquecheck = new javax.swing.JCheckBox();
        shortencheck = new javax.swing.JCheckBox();
        loopcheck = new javax.swing.JCheckBox();
        towardscheck = new javax.swing.JCheckBox();
        directcheck = new javax.swing.JCheckBox();
        localcheck = new javax.swing.JCheckBox();
        routetradingcheck = new javax.swing.JCheckBox();
        updatedataonstartupcheckbox = new javax.swing.JCheckBox();
        updatepricebeforeruncheckbox = new javax.swing.JCheckBox();
        choosepathagain = new javax.swing.JButton();
        cargopanel = new javax.swing.JPanel();
        cargopergood = new javax.swing.JLabel();
        maxcargopergoodtxtfield = new javax.swing.JTextField();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        jLabel2 = new javax.swing.JLabel();
        jPanelstationediting = new javax.swing.JPanel();
        stationedit = new javax.swing.JPanel();
        systemnameupdatetxtfield = new javax.swing.JTextField();
        stationnameupdatetxtfield = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        stationupdateremovebutton = new javax.swing.JButton();
        stationupdateaddbutton = new javax.swing.JButton();
        stationupdateupdatebutton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        stationupdatepadsizecombobox = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        stationupdatelsspinner = new javax.swing.JSpinner();

        jMenuItem4.setText("jMenuItem4");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Trade n't dangerous Gui");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMaximumSize(new java.awt.Dimension(1020, 655));
        setPreferredSize(new java.awt.Dimension(1020, 655));
        setResizable(false);

        jTabbedPane1.setPreferredSize(new java.awt.Dimension(1005, 618));

        consolescrollpane.setAutoscrolls(true);
        consolescrollpane.setDoubleBuffered(true);
        consolescrollpane.setFocusCycleRoot(true);
        consolescrollpane.setMaximumSize(new java.awt.Dimension(696, 32767));

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.setMaximumSize(new java.awt.Dimension(150, 94));
        jTextArea1.setOpaque(false);
        jTextArea1.setPreferredSize(null);
        consolescrollpane.setViewportView(jTextArea1);
        JTextAreaOutputStream out = new JTextAreaOutputStream (jTextArea1);
        System.setOut (new PrintStream (out));

        /*while (true)
        {
            try {
                Thread.sleep (1000L);
            } catch (InterruptedException ex) {
                Logger.getLogger(JTextAreaOutputStream.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/

        manualcommandtxtfield.setText("Type in Manual command here");
        manualcommandtxtfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manualcommandtxtfieldActionPerformed(evt);
            }
        });
        manualcommandtxtfield.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                manualcommandtxtfieldKeyPressed(evt);
            }
        });

        runcommandbutton.setText("run command");
        runcommandbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runcommandbuttonActionPerformed(evt);
            }
        });

        modecombobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "run", "trade", "buy", "sell", "navigate" }));
        modecombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modecomboboxActionPerformed(evt);
            }
        });

        mode.setText("Mode:");

        startpoint.setText("Source:");

        fromtxtfield.setText("example/example");
        fromtxtfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fromtxtfieldActionPerformed(evt);
            }
        });

        destination.setText("Destination:");

        shipsettingscombobox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                shipsettingscomboboxItemStateChanged(evt);
            }
        });
        shipsettingscombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shipsettingscomboboxActionPerformed(evt);
            }
        });

        shipsettings.setText("Shipsettings:");

        generalsettingscombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generalsettingscomboboxActionPerformed(evt);
            }
        });

        generalsettings.setText("General settings:");

        securitysettings.setText("Security settings:");

        Stationcount.setText("Stationcount:");

        Credits.setText("Credits:");

        creditsspinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                creditsspinnerStateChanged(evt);
            }
        });
        creditsspinner.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                creditsspinnerFocusLost(evt);
            }
        });

        securitysettingscombobox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                securitysettingscomboboxItemStateChanged(evt);
            }
        });
        securitysettingscombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                securitysettingscomboboxActionPerformed(evt);
            }
        });

        overridepresetpanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Override (will not be saved)"));

        insurance.setText("Insurance:");

        uniquecheckbox.setText("unique");

        shortencheckbox.setText("shorten");

        loopcheckbox.setText("loop");

        towardscheckbox.setText("towards");

        tradingrouteplanningcheckbox.setText("trading routeplanning");

        updatepricescheckbox.setText("update prices");

        insurancespinner.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                insurancespinnerAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        avoidtext.setText("Avoid:");

        viaoverridetxtfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viaoverridetxtfieldActionPerformed(evt);
            }
        });

        viatext.setText("Via:");

        javax.swing.GroupLayout overridepresetpanelLayout = new javax.swing.GroupLayout(overridepresetpanel);
        overridepresetpanel.setLayout(overridepresetpanelLayout);
        overridepresetpanelLayout.setHorizontalGroup(
            overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(overridepresetpanelLayout.createSequentialGroup()
                .addGroup(overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(overridepresetpanelLayout.createSequentialGroup()
                        .addComponent(tradingrouteplanningcheckbox)
                        .addGap(18, 18, 18)
                        .addComponent(updatepricescheckbox))
                    .addComponent(avoidtext))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, overridepresetpanelLayout.createSequentialGroup()
                .addGroup(overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(overridepresetpanelLayout.createSequentialGroup()
                        .addComponent(insurance)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(insurancespinner, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(overridepresetpanelLayout.createSequentialGroup()
                        .addGroup(overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(uniquecheckbox)
                            .addComponent(viatext))
                        .addGroup(overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(overridepresetpanelLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(viaoverridetxtfield, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(avoidoverridetxtfield, javax.swing.GroupLayout.Alignment.TRAILING)))
                            .addGroup(overridepresetpanelLayout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(shortencheckbox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(loopcheckbox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(towardscheckbox)))))
                .addGap(24, 24, 24))
        );
        overridepresetpanelLayout.setVerticalGroup(
            overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(overridepresetpanelLayout.createSequentialGroup()
                .addGroup(overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(insurance)
                    .addComponent(insurancespinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uniquecheckbox)
                    .addComponent(shortencheckbox)
                    .addComponent(loopcheckbox)
                    .addComponent(towardscheckbox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(avoidoverridetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(avoidtext))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(viaoverridetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(viatext))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(overridepresetpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tradingrouteplanningcheckbox)
                    .addComponent(updatepricescheckbox)))
        );

        saverunsettingtxtfield.setText("filename");

        savebutton.setText("save");
        savebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savebuttonActionPerformed(evt);
            }
        });

        loadrunsettingcombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadrunsettingcomboboxActionPerformed(evt);
            }
        });

        loadbutton.setText("load");
        loadbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadbuttonActionPerformed(evt);
            }
        });

        jButton1.setText("delete");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        startrunbutton.setText("start run");
        startrunbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startrunbuttonActionPerformed(evt);
            }
        });

        jLabel1.setText("additional commands");

        customcommandtxtfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customcommandtxtfieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(startrunbutton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(customcommandtxtfield, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(destination)
                            .addComponent(shipsettings)
                            .addComponent(generalsettings)
                            .addComponent(securitysettings)
                            .addComponent(Stationcount))
                        .addGap(49, 49, 49)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(securitysettingscombobox, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(generalsettingscombobox, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(desttxtfield, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(shipsettingscombobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(stationcountspinner)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(startpoint)
                            .addComponent(Credits)
                            .addComponent(mode))
                        .addGap(95, 95, 95)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(creditsspinner)
                            .addComponent(modecombobox, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(fromtxtfield)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(overridepresetpanel, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(savebutton)
                                .addGap(4, 4, 4)
                                .addComponent(saverunsettingtxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(loadbutton, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(loadrunsettingcombobox, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(modecombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mode))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startpoint)
                    .addComponent(fromtxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(destination)
                    .addComponent(desttxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(shipsettings)
                    .addComponent(shipsettingscombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(generalsettingscombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(generalsettings))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(securitysettings)
                    .addComponent(securitysettingscombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stationcountspinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Stationcount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Credits)
                    .addComponent(creditsspinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(overridepresetpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(customcommandtxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(savebutton)
                    .addComponent(saverunsettingtxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(loadbutton)
                    .addComponent(loadrunsettingcombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(startrunbutton, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(79, 79, 79))
        );

        localpriceupdate.setText("Update prices in local station");
        localpriceupdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                localpriceupdateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelrunLayout = new javax.swing.GroupLayout(jPanelrun);
        jPanelrun.setLayout(jPanelrunLayout);
        jPanelrunLayout.setHorizontalGroup(
            jPanelrunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelrunLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelrunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanelrunLayout.createSequentialGroup()
                        .addComponent(localpriceupdate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(manualcommandtxtfield)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(runcommandbutton))
                    .addComponent(consolescrollpane, javax.swing.GroupLayout.PREFERRED_SIZE, 698, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );
        jPanelrunLayout.setVerticalGroup(
            jPanelrunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelrunLayout.createSequentialGroup()
                .addGroup(jPanelrunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 579, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelrunLayout.createSequentialGroup()
                        .addComponent(consolescrollpane, javax.swing.GroupLayout.PREFERRED_SIZE, 550, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelrunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanelrunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(runcommandbutton)
                                .addComponent(manualcommandtxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(localpriceupdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("run", jPanelrun);

        Securitysettings.setBorder(javax.swing.BorderFactory.createTitledBorder("Security Settings"));
        Securitysettings.setPreferredSize(new java.awt.Dimension(333, 484));

        safeandloadsecuritypanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        loadsecuritybtn.setText("load");
        loadsecuritybtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadsecuritybtnActionPerformed(evt);
            }
        });

        savesecuritybtn.setText("save");
        savesecuritybtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savesecuritybtnActionPerformed(evt);
            }
        });

        savesecurityfilename.setText("example");

        loadsecuritycombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadsecuritycomboboxActionPerformed(evt);
            }
        });

        securitydeletebutton.setText("delete");
        securitydeletebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                securitydeletebuttonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout safeandloadsecuritypanelLayout = new javax.swing.GroupLayout(safeandloadsecuritypanel);
        safeandloadsecuritypanel.setLayout(safeandloadsecuritypanelLayout);
        safeandloadsecuritypanelLayout.setHorizontalGroup(
            safeandloadsecuritypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(safeandloadsecuritypanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(safeandloadsecuritypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(safeandloadsecuritypanelLayout.createSequentialGroup()
                        .addComponent(savesecuritybtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(savesecurityfilename))
                    .addGroup(safeandloadsecuritypanelLayout.createSequentialGroup()
                        .addComponent(loadsecuritybtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(loadsecuritycombobox, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(securitydeletebutton)))
                .addContainerGap())
        );
        safeandloadsecuritypanelLayout.setVerticalGroup(
            safeandloadsecuritypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, safeandloadsecuritypanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(safeandloadsecuritypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(savesecuritybtn)
                    .addComponent(savesecurityfilename, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(safeandloadsecuritypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(loadsecuritybtn)
                    .addComponent(loadsecuritycombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(securitydeletebutton))
                .addContainerGap())
        );

        moneysafetypanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Moneysafety"));
        moneysafetypanel.setPreferredSize(new java.awt.Dimension(200, 100));

        Insurance.setText("Insurance:");

        Percentualinsurance.setText("Percentual Insurance:");

        percentualinsurancetxtfield.setText("0");

        percen1.setText("%");

        Margin.setText("Margin:");

        margintxtfield.setText("0.00");

        insurancesecuritytxtfield.setText("0");

        javax.swing.GroupLayout moneysafetypanelLayout = new javax.swing.GroupLayout(moneysafetypanel);
        moneysafetypanel.setLayout(moneysafetypanelLayout);
        moneysafetypanelLayout.setHorizontalGroup(
            moneysafetypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(moneysafetypanelLayout.createSequentialGroup()
                .addGroup(moneysafetypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(moneysafetypanelLayout.createSequentialGroup()
                        .addComponent(Insurance)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(insurancesecuritytxtfield))
                    .addGroup(moneysafetypanelLayout.createSequentialGroup()
                        .addComponent(Percentualinsurance)
                        .addGap(17, 17, 17)
                        .addComponent(percentualinsurancetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(percen1)
                .addGap(24, 24, 24)
                .addComponent(Margin)
                .addGap(33, 33, 33)
                .addComponent(margintxtfield)
                .addContainerGap())
        );
        moneysafetypanelLayout.setVerticalGroup(
            moneysafetypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(moneysafetypanelLayout.createSequentialGroup()
                .addGroup(moneysafetypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Insurance)
                    .addComponent(Margin)
                    .addComponent(margintxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(insurancesecuritytxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(moneysafetypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Percentualinsurance)
                    .addComponent(percentualinsurancetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(percen1))
                .addGap(0, 5, Short.MAX_VALUE))
        );

        errorpreventionpanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Errorprevention"));
        errorpreventionpanel.setPreferredSize(new java.awt.Dimension(200, 100));

        MinimumStock.setText("Minimum Stock:");

        minstocktextfield.setText("0");

        mindemand.setText("Minimum demand:");

        mindemandtxtfield.setText("0");

        mincrperton.setText("Min Cr per Ton:");

        mincrpertontxtfield.setText("0");

        maxcrperton.setText("Max Cr per Ton");

        maxcrpertontxtfield.setText("0");

        javax.swing.GroupLayout errorpreventionpanelLayout = new javax.swing.GroupLayout(errorpreventionpanel);
        errorpreventionpanel.setLayout(errorpreventionpanelLayout);
        errorpreventionpanelLayout.setHorizontalGroup(
            errorpreventionpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(errorpreventionpanelLayout.createSequentialGroup()
                .addGroup(errorpreventionpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(mincrperton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(MinimumStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(errorpreventionpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(minstocktextfield, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mincrpertontxtfield, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(errorpreventionpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mindemand)
                    .addComponent(maxcrperton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(errorpreventionpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(maxcrpertontxtfield, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mindemandtxtfield, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        errorpreventionpanelLayout.setVerticalGroup(
            errorpreventionpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(errorpreventionpanelLayout.createSequentialGroup()
                .addGroup(errorpreventionpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(MinimumStock)
                    .addComponent(minstocktextfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mindemand)
                    .addComponent(mindemandtxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(errorpreventionpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(errorpreventionpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(mincrpertontxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(mincrperton)
                        .addComponent(maxcrperton))
                    .addComponent(maxcrpertontxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        datasecuritypanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Data"));

        maxdataage.setText("Maximum data-age to calculate with:");

        maxdataagetxtfield.setText("0");

        days.setText("days");

        javax.swing.GroupLayout datasecuritypanelLayout = new javax.swing.GroupLayout(datasecuritypanel);
        datasecuritypanel.setLayout(datasecuritypanelLayout);
        datasecuritypanelLayout.setHorizontalGroup(
            datasecuritypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(datasecuritypanelLayout.createSequentialGroup()
                .addComponent(maxdataage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(maxdataagetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(days)
                .addContainerGap())
        );
        datasecuritypanelLayout.setVerticalGroup(
            datasecuritypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(datasecuritypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(maxdataage)
                .addComponent(maxdataagetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(days))
        );

        avoidviapanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Pirate Protection"));

        avoid.setText("Avoid (seperated with \",\")");

        via.setText("Via (seperated with \",\")");

        avoidtxtfield.setText("example,gold,station,mahonn");

        viatxtfield.setText("example,gold,station,mahonn");

        javax.swing.GroupLayout avoidviapanelLayout = new javax.swing.GroupLayout(avoidviapanel);
        avoidviapanel.setLayout(avoidviapanelLayout);
        avoidviapanelLayout.setHorizontalGroup(
            avoidviapanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(avoidviapanelLayout.createSequentialGroup()
                .addGroup(avoidviapanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(avoid)
                    .addComponent(via))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(avoidviapanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(viatxtfield)
                    .addComponent(avoidtxtfield))
                .addContainerGap())
        );
        avoidviapanelLayout.setVerticalGroup(
            avoidviapanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(avoidviapanelLayout.createSequentialGroup()
                .addGroup(avoidviapanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(avoid)
                    .addComponent(avoidtxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(avoidviapanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(via)
                    .addComponent(viatxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 6, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout SecuritysettingsLayout = new javax.swing.GroupLayout(Securitysettings);
        Securitysettings.setLayout(SecuritysettingsLayout);
        SecuritysettingsLayout.setHorizontalGroup(
            SecuritysettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(moneysafetypanel, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
            .addComponent(errorpreventionpanel, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
            .addComponent(datasecuritypanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(avoidviapanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(safeandloadsecuritypanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 313, Short.MAX_VALUE)
        );
        SecuritysettingsLayout.setVerticalGroup(
            SecuritysettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SecuritysettingsLayout.createSequentialGroup()
                .addComponent(moneysafetypanel, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(errorpreventionpanel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(datasecuritypanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(avoidviapanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(82, 82, 82)
                .addComponent(safeandloadsecuritypanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Shipsettings.setBorder(javax.swing.BorderFactory.createTitledBorder("Ship Settings"));
        Shipsettings.setPreferredSize(new java.awt.Dimension(333, 484));

        saveandloadshippanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        loadshipbtn.setText("load");
        loadshipbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadshipbtnActionPerformed(evt);
            }
        });

        saveshipbtn.setText("safe");
        saveshipbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveshipbtnActionPerformed(evt);
            }
        });

        saveshipfilename.setText("example");

        loadshipcombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadshipcomboboxActionPerformed(evt);
            }
        });

        shipdeletebutton.setText("delete");
        shipdeletebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shipdeletebuttonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout saveandloadshippanel1Layout = new javax.swing.GroupLayout(saveandloadshippanel1);
        saveandloadshippanel1.setLayout(saveandloadshippanel1Layout);
        saveandloadshippanel1Layout.setHorizontalGroup(
            saveandloadshippanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(saveandloadshippanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(saveandloadshippanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(saveandloadshippanel1Layout.createSequentialGroup()
                        .addComponent(saveshipbtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(saveshipfilename))
                    .addGroup(saveandloadshippanel1Layout.createSequentialGroup()
                        .addComponent(loadshipbtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(loadshipcombobox, 0, 169, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(shipdeletebutton)))
                .addContainerGap())
        );
        saveandloadshippanel1Layout.setVerticalGroup(
            saveandloadshippanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, saveandloadshippanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(saveandloadshippanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveshipbtn)
                    .addComponent(saveshipfilename, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(saveandloadshippanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(loadshipbtn)
                    .addComponent(loadshipcombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(shipdeletebutton))
                .addGap(40, 40, 40))
        );

        jumpdistancepanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Jump Distance"));
        jumpdistancepanel.setPreferredSize(new java.awt.Dimension(200, 100));

        unladen.setText("unladen:");

        unladentxtfield.setText("0.00");
        unladentxtfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unladentxtfieldActionPerformed(evt);
            }
        });
        unladentxtfield.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                unladentxtfieldFocusLost(evt);
            }
        });
        unladentxtfield.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                unladentxtfieldInputMethodTextChanged(evt);
            }
        });
        unladentxtfield.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                unladentxtfieldPropertyChange(evt);
            }
        });

        laden.setText("laden:");

        ladentxtfield.setText("0.00");
        ladentxtfield.setMinimumSize(new java.awt.Dimension(6, 38));

        javax.swing.GroupLayout jumpdistancepanelLayout = new javax.swing.GroupLayout(jumpdistancepanel);
        jumpdistancepanel.setLayout(jumpdistancepanelLayout);
        jumpdistancepanelLayout.setHorizontalGroup(
            jumpdistancepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jumpdistancepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(unladen)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(unladentxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(laden)
                .addGap(46, 46, 46)
                .addComponent(ladentxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jumpdistancepanelLayout.setVerticalGroup(
            jumpdistancepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jumpdistancepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jumpdistancepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(unladen)
                    .addComponent(unladentxtfield)
                    .addComponent(laden)
                    .addComponent(ladentxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cargoloadpanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Cargo"));
        cargoloadpanel.setPreferredSize(new java.awt.Dimension(200, 100));

        capacity.setText("max. capacity:");

        capacitytxtfield.setText("0");

        javax.swing.GroupLayout cargoloadpanelLayout = new javax.swing.GroupLayout(cargoloadpanel);
        cargoloadpanel.setLayout(cargoloadpanelLayout);
        cargoloadpanelLayout.setHorizontalGroup(
            cargoloadpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cargoloadpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(capacity)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(capacitytxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        cargoloadpanelLayout.setVerticalGroup(
            cargoloadpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cargoloadpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cargoloadpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(capacity)
                    .addComponent(capacitytxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        shipinsurancepanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Shipinsurance"));
        shipinsurancepanel.setPreferredSize(new java.awt.Dimension(200, 100));

        shipinsurance.setText("insurance:");

        shipinsurancetxtfield.setText("0");

        javax.swing.GroupLayout shipinsurancepanelLayout = new javax.swing.GroupLayout(shipinsurancepanel);
        shipinsurancepanel.setLayout(shipinsurancepanelLayout);
        shipinsurancepanelLayout.setHorizontalGroup(
            shipinsurancepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipinsurancepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(shipinsurance)
                .addGap(18, 18, 18)
                .addComponent(shipinsurancetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        shipinsurancepanelLayout.setVerticalGroup(
            shipinsurancepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipinsurancepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipinsurancepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(shipinsurance)
                    .addComponent(shipinsurancetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        shipsizepanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Shipsize"));
        shipsizepanel.setPreferredSize(new java.awt.Dimension(200, 100));

        shipsizeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "?", "S", "M", "L" }));

        javax.swing.GroupLayout shipsizepanelLayout = new javax.swing.GroupLayout(shipsizepanel);
        shipsizepanel.setLayout(shipsizepanelLayout);
        shipsizepanelLayout.setHorizontalGroup(
            shipsizepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipsizepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(shipsizeComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        shipsizepanelLayout.setVerticalGroup(
            shipsizepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shipsizepanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(shipsizeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout ShipsettingsLayout = new javax.swing.GroupLayout(Shipsettings);
        Shipsettings.setLayout(ShipsettingsLayout);
        ShipsettingsLayout.setHorizontalGroup(
            ShipsettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(saveandloadshippanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jumpdistancepanel, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
            .addComponent(cargoloadpanel, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
            .addComponent(shipinsurancepanel, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
            .addComponent(shipsizepanel, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
        );
        ShipsettingsLayout.setVerticalGroup(
            ShipsettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ShipsettingsLayout.createSequentialGroup()
                .addComponent(jumpdistancepanel, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cargoloadpanel, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(shipinsurancepanel, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(shipsizepanel, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 113, Short.MAX_VALUE)
                .addComponent(saveandloadshippanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Generalsettigns.setBorder(javax.swing.BorderFactory.createTitledBorder("General Settings"));
        Generalsettigns.setPreferredSize(new java.awt.Dimension(333, 484));

        safeandloadgeneralpanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        loadgeneralbtn.setText("load");
        loadgeneralbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadgeneralbtnActionPerformed(evt);
            }
        });

        savegeneralbtn.setText("save");
        savegeneralbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savegeneralbtnActionPerformed(evt);
            }
        });

        savegeneralfilename.setText("example");

        loadgeneralcombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadgeneralcomboboxActionPerformed(evt);
            }
        });

        generaldeletebutton.setText("delete");
        generaldeletebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generaldeletebuttonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout safeandloadgeneralpanelLayout = new javax.swing.GroupLayout(safeandloadgeneralpanel);
        safeandloadgeneralpanel.setLayout(safeandloadgeneralpanelLayout);
        safeandloadgeneralpanelLayout.setHorizontalGroup(
            safeandloadgeneralpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(safeandloadgeneralpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(safeandloadgeneralpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(safeandloadgeneralpanelLayout.createSequentialGroup()
                        .addComponent(savegeneralbtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(savegeneralfilename, javax.swing.GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE))
                    .addGroup(safeandloadgeneralpanelLayout.createSequentialGroup()
                        .addComponent(loadgeneralbtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(loadgeneralcombobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(generaldeletebutton)))
                .addContainerGap())
        );
        safeandloadgeneralpanelLayout.setVerticalGroup(
            safeandloadgeneralpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, safeandloadgeneralpanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(safeandloadgeneralpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(savegeneralbtn)
                    .addComponent(savegeneralfilename, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(safeandloadgeneralpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(loadgeneralbtn)
                    .addComponent(loadgeneralcombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(generaldeletebutton))
                .addContainerGap())
        );

        vebositypanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Grade of Detail"));
        vebositypanel.setPreferredSize(new java.awt.Dimension(200, 100));

        vebosity.setText("vebosity:");

        vebositybox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "null", "v", "vv", "vvv" }));
        vebositybox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vebosityboxActionPerformed(evt);
            }
        });

        debuglevel.setText("debuglevel:");

        debuglevelbox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "null", "w", "ww", "www" }));

        javax.swing.GroupLayout vebositypanelLayout = new javax.swing.GroupLayout(vebositypanel);
        vebositypanel.setLayout(vebositypanelLayout);
        vebositypanelLayout.setHorizontalGroup(
            vebositypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vebositypanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(vebosity)
                .addGap(18, 18, 18)
                .addComponent(vebositybox, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(debuglevel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addComponent(debuglevelbox, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        vebositypanelLayout.setVerticalGroup(
            vebositypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vebositypanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(vebositypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(vebosity)
                    .addComponent(vebositybox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(debuglevel)
                    .addComponent(debuglevelbox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        routedistancepanelpanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Distance Settings"));
        routedistancepanelpanel.setPreferredSize(new java.awt.Dimension(200, 100));

        prunehops.setText("prunehops:");

        prunescore.setText("prunescore:");

        prunehopstxtfield.setText("0");
        prunehopstxtfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prunehopstxtfieldActionPerformed(evt);
            }
        });

        prunescoretxtfield.setText("0.00");
        prunescoretxtfield.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                prunescoretxtfieldPropertyChange(evt);
            }
        });

        LSPenalty.setText("Ls penalty:");

        MaxLS.setText("Max Ls:");

        lspentxtfield.setText("0.00");

        maxlstxtfield.setText("0");

        minhopsbeforeloop.setText("loophops:");

        minhopsbeforelooptxtfield.setText("0");

        jumps.setText("jumps:");

        jumpstxtfield.setText("0");

        javax.swing.GroupLayout routedistancepanelpanelLayout = new javax.swing.GroupLayout(routedistancepanelpanel);
        routedistancepanelpanel.setLayout(routedistancepanelpanelLayout);
        routedistancepanelpanelLayout.setHorizontalGroup(
            routedistancepanelpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(routedistancepanelpanelLayout.createSequentialGroup()
                .addGroup(routedistancepanelpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(prunehops, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(prunescore))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(routedistancepanelpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(prunescoretxtfield, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(prunehopstxtfield))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(routedistancepanelpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LSPenalty)
                    .addComponent(MaxLS))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(routedistancepanelpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lspentxtfield, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(maxlstxtfield))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(routedistancepanelpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(minhopsbeforeloop)
                    .addComponent(jumps))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(routedistancepanelpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jumpstxtfield)
                    .addComponent(minhopsbeforelooptxtfield, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addContainerGap())
        );
        routedistancepanelpanelLayout.setVerticalGroup(
            routedistancepanelpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(routedistancepanelpanelLayout.createSequentialGroup()
                .addGroup(routedistancepanelpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prunehops)
                    .addComponent(prunehopstxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LSPenalty)
                    .addComponent(lspentxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(minhopsbeforeloop)
                    .addComponent(minhopsbeforelooptxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(routedistancepanelpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prunescore)
                    .addComponent(prunescoretxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(MaxLS)
                    .addComponent(maxlstxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jumps)
                    .addComponent(jumpstxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        miscpanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Misc"));

        uniquecheck.setText("unique");
        uniquecheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uniquecheckActionPerformed(evt);
            }
        });

        shortencheck.setText("short ou");

        loopcheck.setText("loop");

        towardscheck.setText("towards");

        directcheck.setText("direct");

        localcheck.setText("local");

        routetradingcheck.setText("trading routeplanning");

        updatedataonstartupcheckbox.setText("Update all data on startup (recommended)");
        updatedataonstartupcheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updatedataonstartupcheckboxActionPerformed(evt);
            }
        });

        updatepricebeforeruncheckbox.setText("Update prices before calculating route (recommended)");

        choosepathagain.setText("choose trade dangerous path again");
        choosepathagain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choosepathagainActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout miscpanelLayout = new javax.swing.GroupLayout(miscpanel);
        miscpanel.setLayout(miscpanelLayout);
        miscpanelLayout.setHorizontalGroup(
            miscpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(miscpanelLayout.createSequentialGroup()
                .addGroup(miscpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(miscpanelLayout.createSequentialGroup()
                        .addGroup(miscpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(uniquecheck)
                            .addComponent(directcheck))
                        .addGap(16, 16, 16)
                        .addGroup(miscpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(shortencheck)
                            .addComponent(localcheck))
                        .addGap(18, 18, 18)
                        .addGroup(miscpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(miscpanelLayout.createSequentialGroup()
                                .addComponent(loopcheck)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(towardscheck))
                            .addGroup(miscpanelLayout.createSequentialGroup()
                                .addComponent(routetradingcheck)
                                .addGap(0, 18, Short.MAX_VALUE))))
                    .addGroup(miscpanelLayout.createSequentialGroup()
                        .addGroup(miscpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(updatepricebeforeruncheckbox)
                            .addGroup(miscpanelLayout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(updatedataonstartupcheckbox)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(choosepathagain, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        miscpanelLayout.setVerticalGroup(
            miscpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(miscpanelLayout.createSequentialGroup()
                .addGroup(miscpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uniquecheck)
                    .addComponent(shortencheck)
                    .addComponent(loopcheck)
                    .addComponent(towardscheck))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(miscpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(directcheck)
                    .addComponent(localcheck)
                    .addComponent(routetradingcheck))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepricebeforeruncheckbox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addComponent(choosepathagain)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatedataonstartupcheckbox))
        );

        cargopanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Cargo"));
        cargopanel.setPreferredSize(new java.awt.Dimension(200, 100));

        cargopergood.setText("Max cargo per good:");

        maxcargopergoodtxtfield.setText("0");

        javax.swing.GroupLayout cargopanelLayout = new javax.swing.GroupLayout(cargopanel);
        cargopanel.setLayout(cargopanelLayout);
        cargopanelLayout.setHorizontalGroup(
            cargopanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cargopanelLayout.createSequentialGroup()
                .addComponent(cargopergood)
                .addGap(18, 18, 18)
                .addComponent(maxcargopergoodtxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        cargopanelLayout.setVerticalGroup(
            cargopanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cargopanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cargopergood)
                .addComponent(maxcargopergoodtxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout GeneralsettignsLayout = new javax.swing.GroupLayout(Generalsettigns);
        Generalsettigns.setLayout(GeneralsettignsLayout);
        GeneralsettignsLayout.setHorizontalGroup(
            GeneralsettignsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(safeandloadgeneralpanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(vebositypanel, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
            .addComponent(routedistancepanelpanel, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
            .addComponent(miscpanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(cargopanel, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
        );
        GeneralsettignsLayout.setVerticalGroup(
            GeneralsettignsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, GeneralsettignsLayout.createSequentialGroup()
                .addComponent(vebositypanel, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(routedistancepanelpanel, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(cargopanel, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(miscpanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(safeandloadgeneralpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLayeredPane1.setPreferredSize(new java.awt.Dimension(0, 100));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/settings headgraphic.jpg"))); // NOI18N

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jLayeredPane1.setLayer(jLabel2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jPanelsettingsLayout = new javax.swing.GroupLayout(jPanelsettings);
        jPanelsettings.setLayout(jPanelsettingsLayout);
        jPanelsettingsLayout.setHorizontalGroup(
            jPanelsettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelsettingsLayout.createSequentialGroup()
                .addGroup(jPanelsettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelsettingsLayout.createSequentialGroup()
                        .addComponent(Shipsettings, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Generalsettigns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Securitysettings, 325, 325, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1000, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        jPanelsettingsLayout.setVerticalGroup(
            jPanelsettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelsettingsLayout.createSequentialGroup()
                .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelsettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Securitysettings, javax.swing.GroupLayout.Alignment.TRAILING, 485, 485, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Generalsettigns, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Shipsettings, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jTabbedPane1.addTab("settings", jPanelsettings);

        stationedit.setBorder(javax.swing.BorderFactory.createTitledBorder("Station Editing"));
        stationedit.setPreferredSize(new java.awt.Dimension(333, 484));

        jLabel3.setText("Systemname");

        jLabel4.setText("Stationname");

        stationupdateremovebutton.setText("remove");
        stationupdateremovebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stationupdateremovebuttonActionPerformed(evt);
            }
        });

        stationupdateaddbutton.setText("add");
        stationupdateaddbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stationupdateaddbuttonActionPerformed(evt);
            }
        });

        stationupdateupdatebutton.setText("update");
        stationupdateupdatebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stationupdateupdatebuttonActionPerformed(evt);
            }
        });

        jLabel5.setText("Padsize");

        stationupdatepadsizecombobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "?", "S", "M", "L" }));

        jLabel6.setText("Lightseconds from next Sun");

        javax.swing.GroupLayout stationeditLayout = new javax.swing.GroupLayout(stationedit);
        stationedit.setLayout(stationeditLayout);
        stationeditLayout.setHorizontalGroup(
            stationeditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stationeditLayout.createSequentialGroup()
                .addGroup(stationeditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(stationeditLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(stationnameupdatetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(stationeditLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(systemnameupdatetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(stationeditLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(129, 129, 129)
                        .addComponent(stationupdatepadsizecombobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(stationeditLayout.createSequentialGroup()
                        .addGroup(stationeditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(stationeditLayout.createSequentialGroup()
                                .addComponent(stationupdateaddbutton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(stationupdateupdatebutton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(stationupdateremovebutton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(stationeditLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(32, 32, 32)
                                .addComponent(stationupdatelsspinner)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        stationeditLayout.setVerticalGroup(
            stationeditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stationeditLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(stationeditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(systemnameupdatetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(stationeditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stationnameupdatetxtfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(stationeditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(stationupdatepadsizecombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(stationeditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(stationupdatelsspinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 292, Short.MAX_VALUE)
                .addGroup(stationeditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stationupdateaddbutton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stationupdateupdatebutton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stationupdateremovebutton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout jPanelstationeditingLayout = new javax.swing.GroupLayout(jPanelstationediting);
        jPanelstationediting.setLayout(jPanelstationeditingLayout);
        jPanelstationeditingLayout.setHorizontalGroup(
            jPanelstationeditingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelstationeditingLayout.createSequentialGroup()
                .addComponent(stationedit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(689, Short.MAX_VALUE))
        );
        jPanelstationeditingLayout.setVerticalGroup(
            jPanelstationeditingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelstationeditingLayout.createSequentialGroup()
                .addContainerGap(106, Short.MAX_VALUE)
                .addComponent(stationedit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jTabbedPane1.addTab("stationedit", jPanelstationediting);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1027, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(454, 454, 454))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void vebosityboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vebosityboxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_vebosityboxActionPerformed

    private void unladentxtfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unladentxtfieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_unladentxtfieldActionPerformed

    private void prunehopstxtfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prunehopstxtfieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_prunehopstxtfieldActionPerformed

    private void prunescoretxtfieldPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_prunescoretxtfieldPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_prunescoretxtfieldPropertyChange

    private void uniquecheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uniquecheckActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_uniquecheckActionPerformed

    private void fromtxtfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fromtxtfieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_fromtxtfieldActionPerformed

    private void viaoverridetxtfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viaoverridetxtfieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_viaoverridetxtfieldActionPerformed

    private void saveshipbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveshipbtnActionPerformed
    String prefix = "shipsetting_";
    String filename = prefix+ saveshipfilename.getText();
    String[] key={
        "Jumpdistance_unladen",unladentxtfield.getText(),
        "Jumpdistance_laden",ladentxtfield.getText(),
        "Maximum_capacity",capacitytxtfield.getText(),
        "Shipinsurance",shipinsurancetxtfield.getText(),
        "Shipsize",shipsizeComboBox.getSelectedItem().toString()
    };//## get the values##//
    
    String[] shipsetting = {//for last used settings
        prefix.substring(0, prefix.length()-1), filename.substring(prefix.length())
    };
    

    
    if (filename != null){
        //conf.setConf(filename,);
            try {
                conf.setConf(filename, key);//save current shipsettings to new config file
                conf.setConf("overall", shipsetting);//save current settings to last used
            } catch (IOException ex) {
                Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
            }
            String[] file={filename,"null"};//make array for configsaver
        try {
            conf.setConf(prefix.substring(0, prefix.length()-1)+"s", file);//ad shipconfig to configslist
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        //## dont get an element in the list twice if saved again! only update values##//
        ComboBoxModel model = shipsettingscombobox.getModel();
        Boolean varcheck = false;
                int size = model.getSize();
                for(int i=0;i<size;i++) {
                    Object element = model.getElementAt(i);
                    if (element.toString().equals(saveshipfilename.getText())){
                        varcheck = true;
                    }
                }
                if(varcheck == false) {
                    shipsettingscombobox.addItem(saveshipfilename.getText());
                    loadshipcombobox.addItem(saveshipfilename.getText());    
                }
                 //choose the new item in the comboboxes for a more fluid feel
                shipsettingscombobox.setSelectedItem(saveshipfilename.getText());
                loadshipcombobox.setSelectedItem(saveshipfilename.getText());
    }else{
        JOptionPane.showMessageDialog(null, "Eggs are not supposed to be square. Get a text in first!", "no File Error", 1);
    }
    // TODO add your handling code here:
    }//GEN-LAST:event_saveshipbtnActionPerformed

    private void unladentxtfieldPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_unladentxtfieldPropertyChange
        
    }//GEN-LAST:event_unladentxtfieldPropertyChange

    private void unladentxtfieldInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_unladentxtfieldInputMethodTextChanged
    // TODO add your handling code here:
    }//GEN-LAST:event_unladentxtfieldInputMethodTextChanged

    private void unladentxtfieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_unladentxtfieldFocusLost
     // TODO add your handling code here:
    }//GEN-LAST:event_unladentxtfieldFocusLost

    private void shipdeletebuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shipdeletebuttonActionPerformed
        String prefix = "shipsetting_";
        conf.deleteconf(prefix + loadshipcombobox.getSelectedItem().toString());    // TODO add your handling code here:
        try {
            conf.deletekey("shipsettings", prefix + loadshipcombobox.getSelectedItem().toString());
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        shipsettingscombobox.removeItem(loadshipcombobox.getSelectedItem());
        loadshipcombobox.removeItem(loadshipcombobox.getSelectedItem());    
    }//GEN-LAST:event_shipdeletebuttonActionPerformed

    private void loadshipbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadshipbtnActionPerformed
        String[] returnvariables;//to put the variables in you will return
        
        String[] shipsetting = {//for last used config
            "shipsetting", loadshipcombobox.getSelectedItem().toString() 
        };
        
        String[] key = {//inicialize array for property.get
        "Jumpdistance_unladen",
        "Jumpdistance_laden",
        "Maximum_capacity",
        "Shipinsurance",
        "Shipsize",    
        };
        
        try { //get vars
            returnvariables = conf.getConf("shipsetting_"+loadshipcombobox.getSelectedItem().toString(), key);        // get the wars into the txtfields so the user can see them
            unladentxtfield.setText(returnvariables[0]);
            ladentxtfield.setText(returnvariables[1]);
            capacitytxtfield.setText(returnvariables[2]);
            shipinsurancetxtfield.setText(returnvariables[3]);
            shipsizeComboBox.setSelectedItem(returnvariables[4]);
            conf.setConf("overall", shipsetting);//get the loaded config into last used running config
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_loadshipbtnActionPerformed

    private void savegeneralbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savegeneralbtnActionPerformed
    String prefix = "generalsetting_";
    String filename = prefix+ savegeneralfilename.getText();
    String[] key={
        "vebosity",vebositybox.getSelectedItem().toString(),
        "debuglevel",debuglevelbox.getSelectedItem().toString(),
        "prunehops",prunehopstxtfield.getText(),
        "lightseconds_penalty",lspentxtfield.getText(),
        "loophops",minhopsbeforelooptxtfield.getText(),
        "prunescore",prunescoretxtfield.getText(),
        "maximum_lightseconds",maxlstxtfield.getText(),
        "jumps",jumpstxtfield.getText(),
        "maximum_cargo_per_good", maxcargopergoodtxtfield.getText(),
        "unique", Boolean.toString(uniquecheck.isSelected()),
        "short_output", Boolean.toString(shortencheck.isSelected()),
        "loop",Boolean.toString(loopcheck.isSelected()),
        "towards",Boolean.toString(towardscheck.isSelected()),
        "direct",Boolean.toString(directcheck.isSelected()),
        "local", Boolean.toString(localcheck.isSelected()),
        "trading_routeplanning",Boolean.toString(routetradingcheck.isSelected()), 
        "update_prices_before_run", Boolean.toString(updatepricebeforeruncheckbox.isSelected()),
    };//## get the values##//
    
    String[] generalsetting = {//for last used settings
        prefix.substring(0, prefix.length()-1), filename.substring(prefix.length())
    };
    

    
    if (filename != null){
        //conf.setConf(filename,);
            try {
                conf.setConf(filename, key);//save current shipsettings to new config file
                conf.setConf("overall", generalsetting);//save current settings to last used
            } catch (IOException ex) {
                Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
            }
            String[] file={filename,"null"};//make array for configsaver
        try {
            conf.setConf(prefix.substring(0, prefix.length()-1)+"s", file);//ad generalconfig to configslist
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //## dont get an element in the list twice if saved again! only update values##//
        ComboBoxModel model = generalsettingscombobox.getModel();
        Boolean varcheck = false;
                int size = model.getSize();
                for(int i=0;i<size;i++) {
                    Object element = model.getElementAt(i);
                    if (element.toString().equals(savegeneralfilename.getText())){
                        varcheck = true;
                    }
                }
                if(varcheck == false) {
                    generalsettingscombobox.addItem(savegeneralfilename.getText());
                    loadgeneralcombobox.addItem(savegeneralfilename.getText());    
                }
                //choose the new item in the comboboxes for a more fluid feel
                generalsettingscombobox.setSelectedItem(savegeneralfilename.getText());
                loadgeneralcombobox.setSelectedItem(savegeneralfilename.getText());
    }else{
        JOptionPane.showMessageDialog(null, "Eggs are not supposed to be square. Get a text in first!", "no File Error", 1);
    }    // TODO add your handling code here:
    }//GEN-LAST:event_savegeneralbtnActionPerformed

    private void loadgeneralbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadgeneralbtnActionPerformed
        String[] returnvariables;//to put the variables in you will return
        
        String[] generalsetting = {//for last used config
            "generalsetting", loadgeneralcombobox.getSelectedItem().toString() 
        };
        
        String[] key = {//inicialize array for property.get
        "vebosity",
        "debuglevel",
        "prunehops",
        "lightseconds_penalty",
        "loophops",
        "prunescore",
        "maximum_lightseconds",
        "jumps",
        "maximum_cargo_per_good",
        "unique",
        "short_output",
        "loop",
        "towards",
        "direct",
        "local",
        "trading_routeplanning", 
        "update_prices_before_run"
        };
        
        try { //get vars
            returnvariables = conf.getConf("generalsetting_"+loadgeneralcombobox.getSelectedItem().toString(), key);        // get the wars into the txtfields so the user can see them
            vebositybox.setSelectedItem(returnvariables[0]);
            debuglevelbox.setSelectedItem(returnvariables[1]);
            prunehopstxtfield.setText(returnvariables[2]);
            lspentxtfield.setText(returnvariables[3]);
            minhopsbeforelooptxtfield.setText(returnvariables[4]);
            prunescoretxtfield.setText(returnvariables[5]);
            maxlstxtfield.setText(returnvariables[6]);
            jumpstxtfield.setText(returnvariables[7]);
            maxcargopergoodtxtfield.setText(returnvariables[8]);
            uniquecheck.setSelected("true".equals(returnvariables[9]));
            shortencheck.setSelected("true".equals(returnvariables[10]));
            loopcheck.setSelected("true".equals(returnvariables[11]));
            towardscheck.setSelected("true".equals(returnvariables[12]));
            directcheck.setSelected("true".equals(returnvariables[13]));
            localcheck.setSelected("true".equals(returnvariables[14]));
            routetradingcheck.setSelected("true".equals(returnvariables[15]));
            routetradingcheck.setSelected("true".equals(returnvariables[16]));
            conf.setConf("overall", generalsetting);//get the loaded config into last used running config
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }        // TODO add your handling code here:
    }//GEN-LAST:event_loadgeneralbtnActionPerformed

    private void savesecuritybtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savesecuritybtnActionPerformed
    String prefix = "securitysetting_";
    String filename = prefix+ savesecurityfilename.getText();
    String[] key={
        "Insurance", insurancesecuritytxtfield.getText(),
        "Margin_%", margintxtfield.getText(),
        "Insurance_%", percentualinsurancetxtfield.getText(),
        "Minimum_stock", minstocktextfield.getText(),
        "Minimum_demand", mindemandtxtfield.getText(),
        "Minimum_cr_per_ton",mincrpertontxtfield.getText(),
        "Maximum_cr_per_ton", maxcrpertontxtfield.getText(),
        "Maximum_data_age", maxdataagetxtfield.getText(),
        "Avoid","",
        "Via",""};//## get the values##//
    
        //if avoid/via != example; fill value,
        if (avoidtxtfield.getText().equals("example,gold,station,mahonn")==false) {
            key[17] = avoidtxtfield.getText();
        }
        if (viatxtfield.getText().equals("example,gold,station,mahonn")==false){
            key[19] = viatxtfield.getText();
        }

    String[] securitysetting = {//for last used settings
        prefix.substring(0, prefix.length()-1), filename.substring(prefix.length())
    };
    

    
    if (filename != null){
        //conf.setConf(filename,);
            try {
                conf.setConf(filename, key);//save current shipsettings to new config file
                conf.setConf("overall", securitysetting);//save current settings to last used
            } catch (IOException ex) {
                Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
            }
            String[] file={filename,"null"};//make array for configsaver
        try {
            conf.setConf(prefix.substring(0, prefix.length()-1)+"s", file);//ad generalconfig to configslist
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //## dont get an element in the list twice if saved again! only update values##//
        ComboBoxModel model = securitysettingscombobox.getModel();
        Boolean varcheck = false;
                int size = model.getSize();
                for(int i=0;i<size;i++) {
                    Object element = model.getElementAt(i);
                    if (element.toString().equals(savesecurityfilename.getText())){
                        varcheck = true;
                    }
                }
                if(varcheck == false) {
                    securitysettingscombobox.addItem(savesecurityfilename.getText());
                    loadsecuritycombobox.addItem(savesecurityfilename.getText());    
                }
                
                 //choose the new item in the comboboxes for a more fluid feel
                securitysettingscombobox.setSelectedItem(savegeneralfilename.getText());
                loadsecuritycombobox.setSelectedItem(savegeneralfilename.getText());
    }else{
        JOptionPane.showMessageDialog(null, "Eggs are not supposed to be square. Get a text in first!", "no File Error", 1);
    }         // TODO add your handling code here:
    }//GEN-LAST:event_savesecuritybtnActionPerformed

    private void generaldeletebuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generaldeletebuttonActionPerformed
        String prefix = "generalsetting_";
        conf.deleteconf(prefix + loadgeneralcombobox.getSelectedItem().toString());    // TODO add your handling code here:
        try {
            conf.deletekey("generalsettings", prefix + loadgeneralcombobox.getSelectedItem().toString());
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        generalsettingscombobox.removeItem(loadgeneralcombobox.getSelectedItem());
        loadgeneralcombobox.removeItem(loadgeneralcombobox.getSelectedItem());        // TODO add your handling code here:
        
    }//GEN-LAST:event_generaldeletebuttonActionPerformed

    private void securitydeletebuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_securitydeletebuttonActionPerformed
    String prefix = "securitysetting_";
        conf.deleteconf(prefix + loadsecuritycombobox.getSelectedItem().toString());    // TODO add your handling code here:
        try {
            conf.deletekey("securitysettings", prefix + loadsecuritycombobox.getSelectedItem().toString());
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        securitysettingscombobox.removeItem(loadsecuritycombobox.getSelectedItem());
        loadsecuritycombobox.removeItem(loadsecuritycombobox.getSelectedItem());    // TODO add your handling code here:
    }//GEN-LAST:event_securitydeletebuttonActionPerformed

    private void loadsecuritybtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadsecuritybtnActionPerformed
    String[] returnvariables;//to put the variables in you will return
        
        String[] securitysetting = {//for last used config
            "securitysetting", loadsecuritycombobox.getSelectedItem().toString() 
        };
        
        String[] key = {//inicialize array for property.get
            "Minimum_demand",
            "Minimum_cr_per_ton",
            "Insurance_%",
            "Maximum_data_age",
            "Minimum_stock",
            "Margin_%",
            "Avoid",
            "Maximum_cr_per_ton",
            "Insurance",
            "Via",
        };
        
        try { //get vars
            returnvariables = conf.getConf("securitysetting_"+loadsecuritycombobox.getSelectedItem().toString(), key);        // get the wars into the txtfields so the user can see them
            mindemandtxtfield.setText(returnvariables[0]);
            mincrpertontxtfield.setText(returnvariables[1]);
            percentualinsurancetxtfield.setText(returnvariables[2]);
            maxdataagetxtfield.setText(returnvariables[3]);
            minstocktextfield.setText(returnvariables[4]);
            margintxtfield.setText(returnvariables[5]);
            avoidtxtfield.setText(returnvariables[6]);
            maxcrpertontxtfield.setText(returnvariables[7]);
            insurancesecuritytxtfield.setText(returnvariables[8]);
            viatxtfield.setText(returnvariables[9]);
            conf.setConf("overall", securitysetting);//get the loaded config into last used running config
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }    // TODO add your handling code here:
    }//GEN-LAST:event_loadsecuritybtnActionPerformed

    private void savebuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savebuttonActionPerformed
String prefix = "runsetting_";
    String filename = prefix+ saverunsettingtxtfield.getText();
    String[] key={
        "Mode",modecombobox.getSelectedItem().toString(),
        "Source", fromtxtfield.getText(),
        "Destination",desttxtfield.getText(),
        "Shipsettings",shipsettingscombobox.getSelectedItem().toString(),
        "General_settings",generalsettingscombobox.getSelectedItem().toString(),
        "Security_settings",securitysettingscombobox.getSelectedItem().toString(),
        "Stationcount", stationcountspinner.getValue().toString(),
        "Credits", creditsspinner.getValue().toString(),
        "Additional_commands",customcommandtxtfield.getText(),
    };//## get the values##//
    
    String[] runsetting = {//for last used settings
        prefix.substring(0, prefix.length()-1), filename.substring(prefix.length())
    };
    

    
    if (filename != null){
        //conf.setConf(filename,);
            try {
                conf.setConf(filename, key);//save current shipsettings to new config file
                conf.setConf("overall", runsetting);//save current settings to last used
            } catch (IOException ex) {
                Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
            }
            String[] file={filename,"null"};//make array for configsaver
        try {
            conf.setConf(prefix.substring(0, prefix.length()-1)+"s", file);//ad shipconfig to configslist
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        
        //## dont get an element in the list twice if saved again! only update values##//
        ComboBoxModel model = loadrunsettingcombobox.getModel();
        Boolean varcheck = false;
                int size = model.getSize();
                for(int i=0;i<size;i++) {
                    Object element = model.getElementAt(i);
                    if (element.toString().equals(saverunsettingtxtfield.getText())){
                        varcheck = true;
                    }
                }
                if(varcheck == false) {
                    loadrunsettingcombobox.addItem(saverunsettingtxtfield.getText());   
                } 
                loadrunsettingcombobox.setSelectedItem(saverunsettingtxtfield.getText());
    }else{
        JOptionPane.showMessageDialog(null, "Eggs are not supposed to be square. Get a text in first!", "no File Error", 1);
    }        // TODO add your handling code here:
    }//GEN-LAST:event_savebuttonActionPerformed

    private void loadbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadbuttonActionPerformed
        String[] returnvariables;//to put the variables in you will return
        
        String[] runsetting = {//for last used config
            "runsetting", loadrunsettingcombobox.getSelectedItem().toString() 
        };
        
        String[] key = {//inicialize array for property.get
        "Mode",
        "Source",
        "Destination",
        "Shipsettings",
        "General_settings",
        "Security_settings",
        "Stationcount",
        "Credits",
        "Item",
        "Additional_commands",
        };
        
        try { //get vars
            returnvariables = conf.getConf("runsetting_"+loadrunsettingcombobox.getSelectedItem().toString(), key);        // get the wars into the txtfields so the user can see them
            modecombobox.setSelectedItem(returnvariables[0]);
            fromtxtfield.setText(returnvariables[1]);
            desttxtfield.setText(returnvariables[2]);
            shipsettingscombobox.setSelectedItem(returnvariables[3]);
            generalsettingscombobox.setSelectedItem(returnvariables[4]);
            securitysettingscombobox.setSelectedItem(returnvariables[5]);
            stationcountspinner.setValue(Integer.parseInt(returnvariables[6]));
            creditsspinner.setValue(Integer.parseInt(returnvariables[7]));
            customcommandtxtfield.setText(returnvariables[9]);
            conf.setConf("overall", runsetting);//get the loaded config into last used running config
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_loadbuttonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String prefix = "runsetting_";
        conf.deleteconf(prefix + loadrunsettingcombobox.getSelectedItem().toString());    // TODO add your handling code here:
        try {
            conf.deletekey("runsettings", prefix + loadrunsettingcombobox.getSelectedItem().toString());
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        loadrunsettingcombobox.removeItem(loadrunsettingcombobox.getSelectedItem());    // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void choosepathagainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choosepathagainActionPerformed
    ChoosePath choosepath = new ChoosePath();
                String[] key = {"path", choosepath.Filechoose()};
        try {
            conf.setConf("overall", key); // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_choosepathagainActionPerformed

    private void startrunbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startrunbuttonActionPerformed
    if ("run".equals(modecombobox.getSelectedItem())){  
        try {
            String[] mergeconfs = {
              "shipsetting_"+shipsettingscombobox.getSelectedItem().toString(),
              "generalsetting_"+generalsettingscombobox.getSelectedItem().toString(),
              "securitysetting_"+securitysettingscombobox.getSelectedItem().toString()
            };
            conf.settingsmerger(mergeconfs, "tmp");
            String shipsize = null;//shipsite must be edited
            if (conf.getsinglevalue("tmp", "Shipsize").equals("L")){
                shipsize = "L";
            } else if (conf.getsinglevalue("tmp", "Shipsize").equals("M")){
                shipsize = "ML";
            }else if (conf.getsinglevalue("tmp", "Shipsize").equals("S")){
                shipsize = "SML";
            } else {
                shipsize = "?";
            }
            
            command.run(
            updatepricescheckbox.isSelected(),//updatebefore run boolean
            fromtxtfield.getText(),//from string
            Integer.parseInt(stationcountspinner.getValue().toString()),//hops int
            desttxtfield.getText(),//destination string
            Float.parseFloat(conf.getsinglevalue("tmp", "Jumpdistance_laden")),//jumpdistance laden float
            Float.parseFloat(conf.getsinglevalue("tmp", "Jumpdistance_unladen")),//jumpdistance unladen float
            Integer.parseInt(conf.getsinglevalue("tmp", "Maximum_capacity")),// capacity int
            shipsize,//shipsize string
            Float.parseFloat(conf.getsinglevalue("tmp", "Margin_%")),//float margin
            Integer.parseInt(conf.getsinglevalue("tmp", "Minimum_stock")),//int minstock
            Integer.parseInt(conf.getsinglevalue("tmp", "Minimum_demand")),//int mindemand
            Integer.parseInt(conf.getsinglevalue("tmp", "Minimum_cr_per_ton")),//int min cr per ton
            Integer.parseInt(conf.getsinglevalue("tmp", "Maximum_cr_per_ton")),//int max cr per ton
            Float.parseFloat(conf.getsinglevalue("tmp", "Maximum_data_age")),//float max data age
            avoidoverridetxtfield.getText(),//string avoid
            viaoverridetxtfield.getText(),//sting via
            Integer.parseInt(conf.getsinglevalue("tmp", "prunehops")),//int prunehops
            Float.parseFloat(conf.getsinglevalue("tmp", "prunescore")),//float prunescore
            Float.parseFloat(conf.getsinglevalue("tmp", "lightseconds_penalty")),//float ls penalty
            Integer.parseInt(conf.getsinglevalue("tmp", "loophops")),//int loophops
            Integer.parseInt(conf.getsinglevalue("tmp", "maximum_lightseconds")),//int maxls
            Integer.parseInt(conf.getsinglevalue("tmp", "jumps")),//int jumps
            Integer.parseInt(conf.getsinglevalue("tmp", "maximum_cargo_per_good")),//int maxcargopergood
            uniquecheckbox.isSelected(),//boolean unique
            shortencheckbox.isSelected(),//boolean shorten
            loopcheckbox.isSelected(),//boolean loop
            Boolean.parseBoolean(conf.getsinglevalue("tmp", "direct")),//boolean direct
            Integer.parseInt(creditsspinner.getValue().toString()),//int credits
            customcommandtxtfield.getText(),//string customcode
            conf.getsinglevalue("tmp", "vebosity"),//string vebosety
            conf.getsinglevalue("tmp", "debuglevel"),//string debuglevel
            Integer.parseInt(insurancespinner.getValue().toString()));//int insurance
            conf.deleteconf("tmp");
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }else if ("trade".equals(modecombobox.getSelectedItem())){
        
    }else if ("buy".equals(modecombobox.getSelectedItem())){
        try {
            String[] mergeconfs = {
              "shipsetting_"+shipsettingscombobox.getSelectedItem().toString(),
              "generalsetting_"+generalsettingscombobox.getSelectedItem().toString(),
              "securitysetting_"+securitysettingscombobox.getSelectedItem().toString()
            };
            conf.settingsmerger(mergeconfs, "tmp");
            String shipsize = null;//shipsite must be edited
            if (conf.getsinglevalue("tmp", "Shipsize").equals("L")){
                shipsize = "L";
            } else if (conf.getsinglevalue("tmp", "Shipsize").equals("M")){
                shipsize = "ML";
            }else if (conf.getsinglevalue("tmp", "Shipsize").equals("S")){
                shipsize = "SML";
            } else {
                shipsize = "?";
            }
            command.buy(
                    updatepricescheckbox.isSelected(),//updatebefore run boolean
                    Integer.parseInt(stationcountspinner.getValue().toString()),//itemcount
                    Integer.parseInt(conf.getsinglevalue("tmp", "Minimum_stock")),//min stock
                    fromtxtfield.getText(), //near
                    Integer.parseInt(conf.getsinglevalue("tmp", "maximum_lightseconds")), //maximum distance
                    avoidtxtfield.getText(), //avoid
                    shipsize,//shipsize string
                    uniquecheckbox.isSelected(), //onehop
                    Integer.parseInt(creditsspinner.getValue().toString()),//max credits
                    shortencheckbox.isSelected(), // sortprices
                    loopcheckbox.isSelected(), //supplysort
                    towardscheckbox.isSelected(),//blackmarket
                    desttxtfield.getText());//items
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }else if ("sell".equals(modecombobox.getSelectedItem())){
        
    }else if ("navigate".equals(modecombobox.getSelectedItem())){
        
    }
    }//GEN-LAST:event_startrunbuttonActionPerformed

    private void modecomboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modecomboboxActionPerformed
    String mode = modecombobox.getSelectedItem().toString();
    if (mode.equals("buy")){
        Stationcount.setText("itemcount");
        Credits.setText("max Price");
        destination.setText("Items");
        startpoint.setText("near");
        uniquecheckbox.setText("one-stop");
        shortencheckbox.setText("pricesort");
        loopcheck.setText("sort");
        towardscheckbox.setText("blackm.");
        
        
    }
    }//GEN-LAST:event_modecomboboxActionPerformed

    private void shipsettingscomboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shipsettingscomboboxActionPerformed
    
    }//GEN-LAST:event_shipsettingscomboboxActionPerformed

    private void insurancespinnerAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_insurancespinnerAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_insurancespinnerAncestorAdded

    private void securitysettingscomboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_securitysettingscomboboxActionPerformed
    String pre = "securitysetting_";
        try {
            String avo = conf.getsinglevalue(pre + securitysettingscombobox.getSelectedItem().toString(), "Avoid");
            String via = conf.getsinglevalue(pre + securitysettingscombobox.getSelectedItem().toString(), "Via");
            avoidoverridetxtfield.setText(avo);
            viaoverridetxtfield.setText(via);
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_securitysettingscomboboxActionPerformed

    private void creditsspinnerFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_creditsspinnerFocusLost
  // TODO add your handling code here:
    }//GEN-LAST:event_creditsspinnerFocusLost

    private void shipsettingscomboboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_shipsettingscomboboxItemStateChanged
    try {
            updateinsurancespinner();        
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }    // TODO add your handling code here:
    }//GEN-LAST:event_shipsettingscomboboxItemStateChanged

    private void securitysettingscomboboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_securitysettingscomboboxItemStateChanged
    try {
            updateinsurancespinner();        // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }    // TODO add your handling code here:
    }//GEN-LAST:event_securitysettingscomboboxItemStateChanged

    private void creditsspinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_creditsspinnerStateChanged
    try {
            updateinsurancespinner();        // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }    // TODO add your handling code here:
    }//GEN-LAST:event_creditsspinnerStateChanged

    private void runcommandbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runcommandbuttonActionPerformed
    CmdStarter cmd = new CmdStarter();
    String[] command = {"trade.py "+manualcommandtxtfield.getText(),};
        try {
            cmd.start(command);// TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_runcommandbuttonActionPerformed

    private void updatedataonstartupcheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updatedataonstartupcheckboxActionPerformed
        try {
            conf.setsingleConf("overall", "update_on_startup", String.valueOf(updatedataonstartupcheckbox.isSelected()));    // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_updatedataonstartupcheckboxActionPerformed

    private void loadshipcomboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadshipcomboboxActionPerformed
    saveshipfilename.setText(loadshipcombobox.getSelectedItem().toString());    // TODO add your handling code here:
    }//GEN-LAST:event_loadshipcomboboxActionPerformed

    private void loadgeneralcomboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadgeneralcomboboxActionPerformed
    savegeneralfilename.setText(loadgeneralcombobox.getSelectedItem().toString());    // TODO add your handling code here:
    }//GEN-LAST:event_loadgeneralcomboboxActionPerformed

    private void loadsecuritycomboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadsecuritycomboboxActionPerformed
    savesecurityfilename.setText(loadsecuritycombobox.getSelectedItem().toString());    // TODO add your handling code here:
    }//GEN-LAST:event_loadsecuritycomboboxActionPerformed

    private void customcommandtxtfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customcommandtxtfieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customcommandtxtfieldActionPerformed

    private void loadrunsettingcomboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadrunsettingcomboboxActionPerformed
    saverunsettingtxtfield.setText(loadrunsettingcombobox.getSelectedItem().toString());    // TODO add your handling code here:
    }//GEN-LAST:event_loadrunsettingcomboboxActionPerformed

    private void manualcommandtxtfieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_manualcommandtxtfieldKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_manualcommandtxtfieldKeyPressed

    private void manualcommandtxtfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manualcommandtxtfieldActionPerformed
        runcommandbuttonActionPerformed(evt);// if backspace secd command:
    }//GEN-LAST:event_manualcommandtxtfieldActionPerformed

    private void generalsettingscomboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generalsettingscomboboxActionPerformed
        try {
            String pre = "generalsetting_";
            String un = conf.getsinglevalue(pre+generalsettingscombobox.getSelectedItem().toString(), "unique");
            String sh = conf.getsinglevalue(pre+generalsettingscombobox.getSelectedItem().toString(), "short_output");
            String lo = conf.getsinglevalue(pre+generalsettingscombobox.getSelectedItem().toString(), "loop");
            String to = conf.getsinglevalue(pre+generalsettingscombobox.getSelectedItem().toString(), "towards");
            String up = conf.getsinglevalue(pre+generalsettingscombobox.getSelectedItem().toString(), "update_prices_before_run");
            if (un.equals("true")){
               uniquecheckbox.setSelected(true); 
            } else {
               uniquecheckbox.setSelected(false);     
            }
            if (sh.equals("true")){
               shortencheckbox.setSelected(true); 
            } else {
               shortencheckbox.setSelected(false);     
            }
            if (lo.equals("true")){
               loopcheckbox.setSelected(true); 
            } else {
               loopcheckbox.setSelected(false);     
            }
            if (to.equals("true")){
               towardscheckbox.setSelected(true); 
            } else {
               towardscheckbox.setSelected(false);     
            }
            if (up.equals("true")){
               updatepricescheckbox.setSelected(true); 
            } else {
               updatepricescheckbox.setSelected(false);     
            }
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
            // TODO add your handling code here:
    }//GEN-LAST:event_generalsettingscomboboxActionPerformed

    private void localpriceupdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_localpriceupdateActionPerformed
        try {                                               
            CmdStarter cmd = new CmdStarter();
            String path = null;
                path = conf.getsinglevalue("overall", "path");
                File file = new File(path+"\\data\\edapi.cookies");
                if (file.exists()==false){
                    String message = "Please log in the first time manually by using \"trade.py import --plug edapi \" over the CMD!";
                    JOptionPane.showMessageDialog(cargopanel, message);
                }
                else{
                    cmd.startSingleCommand("trade.py imp -P edapi -O eddn");
                }
                
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_localpriceupdateActionPerformed

    private void stationupdateupdatebuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stationupdateupdatebuttonActionPerformed
        try {
            Commandbuilder command = new Commandbuilder();
            command.stationupdate(
                    systemnameupdatetxtfield.getText(),
                    stationnameupdatetxtfield.getText(),
                    stationupdatepadsizecombobox.getSelectedItem().toString(),
                    Integer.parseInt(stationupdatelsspinner.getValue().toString())
                    );
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        

    }//GEN-LAST:event_stationupdateupdatebuttonActionPerformed

    private void stationupdateaddbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stationupdateaddbuttonActionPerformed
        try {
            Commandbuilder command = new Commandbuilder();
            command.stationadd(
                    systemnameupdatetxtfield.getText(),
                    stationnameupdatetxtfield.getText(),
                    stationupdatepadsizecombobox.getSelectedItem().toString(),
                    Integer.parseInt(stationupdatelsspinner.getValue().toString())
                    );
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_stationupdateaddbuttonActionPerformed

    private void stationupdateremovebuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stationupdateremovebuttonActionPerformed
        try {
            Commandbuilder command = new Commandbuilder();
            command.stationremove(
                    systemnameupdatetxtfield.getText(),
                    stationnameupdatetxtfield.getText()
                    );
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }  // TODO add your handling code here:
    }//GEN-LAST:event_stationupdateremovebuttonActionPerformed
    private void updateinsurancespinner() throws IOException{
    String[] returnvariables;
    //get the settings
    //get percentualinsurance
        try {
            String[] key = {
            "Insurance_%", "Insurance"
            };
            returnvariables = conf.getConf("securitysetting_"+securitysettingscombobox.getSelectedItem().toString(), key); 
            double percentual = Integer.parseInt(returnvariables[0]);
            percentual = percentual / 100;
            double insuranceint = Integer.parseInt(returnvariables[1]);
            key[0] = "Shipinsurance";
            key[1] = "Shipsize"; //prevent null pointer ex?
            returnvariables = conf.getConf("shipsetting_"+shipsettingscombobox.getSelectedItem().toString(), key);
            double shipinsuranceint = Integer.parseInt(returnvariables[0]);
            double credits = Integer.parseInt(creditsspinner.getValue().toString());
             //calculation
            double finalvalue = (credits * percentual + insuranceint + shipinsuranceint);
            insurancespinner.setValue((int)(finalvalue));
        } catch (Exception e) {
        }   
}
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Interface().setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Credits;
    private javax.swing.JPanel Generalsettigns;
    private javax.swing.JLabel Insurance;
    private javax.swing.JLabel LSPenalty;
    private javax.swing.JLabel Margin;
    private javax.swing.JLabel MaxLS;
    private javax.swing.JLabel MinimumStock;
    private javax.swing.JLabel Percentualinsurance;
    private javax.swing.JPanel Securitysettings;
    private javax.swing.JPanel Shipsettings;
    private javax.swing.JLabel Stationcount;
    private javax.swing.JLabel avoid;
    private javax.swing.JTextField avoidoverridetxtfield;
    private javax.swing.JLabel avoidtext;
    private javax.swing.JTextField avoidtxtfield;
    private javax.swing.JPanel avoidviapanel;
    private javax.swing.JLabel capacity;
    private javax.swing.JTextField capacitytxtfield;
    private javax.swing.JPanel cargoloadpanel;
    private javax.swing.JPanel cargopanel;
    private javax.swing.JLabel cargopergood;
    private javax.swing.JButton choosepathagain;
    private javax.swing.JScrollPane consolescrollpane;
    private javax.swing.JSpinner creditsspinner;
    private javax.swing.JTextField customcommandtxtfield;
    private javax.swing.JPanel datasecuritypanel;
    private javax.swing.JLabel days;
    private javax.swing.JLabel debuglevel;
    private javax.swing.JComboBox debuglevelbox;
    private javax.swing.JLabel destination;
    private javax.swing.JTextField desttxtfield;
    private javax.swing.JCheckBox directcheck;
    private javax.swing.JPanel errorpreventionpanel;
    private javax.swing.JTextField fromtxtfield;
    private javax.swing.JButton generaldeletebutton;
    private javax.swing.JLabel generalsettings;
    private javax.swing.JComboBox generalsettingscombobox;
    private javax.swing.JLabel insurance;
    private javax.swing.JTextField insurancesecuritytxtfield;
    private javax.swing.JSpinner insurancespinner;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelrun;
    private javax.swing.JPanel jPanelsettings;
    private javax.swing.JPanel jPanelstationediting;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JPanel jumpdistancepanel;
    private javax.swing.JLabel jumps;
    private javax.swing.JTextField jumpstxtfield;
    private javax.swing.JLabel laden;
    private javax.swing.JTextField ladentxtfield;
    private javax.swing.JButton loadbutton;
    private javax.swing.JButton loadgeneralbtn;
    private javax.swing.JComboBox loadgeneralcombobox;
    private javax.swing.JComboBox loadrunsettingcombobox;
    private javax.swing.JButton loadsecuritybtn;
    private javax.swing.JComboBox loadsecuritycombobox;
    private javax.swing.JButton loadshipbtn;
    private javax.swing.JComboBox loadshipcombobox;
    private javax.swing.JCheckBox localcheck;
    private javax.swing.JButton localpriceupdate;
    private javax.swing.JCheckBox loopcheck;
    private javax.swing.JCheckBox loopcheckbox;
    private javax.swing.JTextField lspentxtfield;
    private javax.swing.JTextField manualcommandtxtfield;
    private javax.swing.JTextField margintxtfield;
    private javax.swing.JTextField maxcargopergoodtxtfield;
    private javax.swing.JLabel maxcrperton;
    private javax.swing.JTextField maxcrpertontxtfield;
    private javax.swing.JLabel maxdataage;
    private javax.swing.JTextField maxdataagetxtfield;
    private javax.swing.JTextField maxlstxtfield;
    private javax.swing.JLabel mincrperton;
    private javax.swing.JTextField mincrpertontxtfield;
    private javax.swing.JLabel mindemand;
    private javax.swing.JTextField mindemandtxtfield;
    private javax.swing.JLabel minhopsbeforeloop;
    private javax.swing.JTextField minhopsbeforelooptxtfield;
    private javax.swing.JTextField minstocktextfield;
    private javax.swing.JPanel miscpanel;
    private javax.swing.JLabel mode;
    private javax.swing.JComboBox modecombobox;
    private javax.swing.JPanel moneysafetypanel;
    private javax.swing.JPanel overridepresetpanel;
    private javax.swing.JLabel percen1;
    private javax.swing.JTextField percentualinsurancetxtfield;
    private javax.swing.JLabel prunehops;
    private javax.swing.JTextField prunehopstxtfield;
    private javax.swing.JLabel prunescore;
    private javax.swing.JTextField prunescoretxtfield;
    private javax.swing.JPanel routedistancepanelpanel;
    private javax.swing.JCheckBox routetradingcheck;
    private javax.swing.JButton runcommandbutton;
    private javax.swing.JPanel safeandloadgeneralpanel;
    private javax.swing.JPanel safeandloadsecuritypanel;
    private javax.swing.JPanel saveandloadshippanel1;
    private javax.swing.JButton savebutton;
    private javax.swing.JButton savegeneralbtn;
    private javax.swing.JTextField savegeneralfilename;
    private javax.swing.JTextField saverunsettingtxtfield;
    private javax.swing.JButton savesecuritybtn;
    private javax.swing.JTextField savesecurityfilename;
    private javax.swing.JButton saveshipbtn;
    private javax.swing.JTextField saveshipfilename;
    private javax.swing.JButton securitydeletebutton;
    private javax.swing.JLabel securitysettings;
    private javax.swing.JComboBox securitysettingscombobox;
    private javax.swing.JButton shipdeletebutton;
    private javax.swing.JLabel shipinsurance;
    private javax.swing.JPanel shipinsurancepanel;
    private javax.swing.JTextField shipinsurancetxtfield;
    private javax.swing.JLabel shipsettings;
    private javax.swing.JComboBox shipsettingscombobox;
    private javax.swing.JComboBox shipsizeComboBox;
    private javax.swing.JPanel shipsizepanel;
    private javax.swing.JCheckBox shortencheck;
    private javax.swing.JCheckBox shortencheckbox;
    private javax.swing.JLabel startpoint;
    private javax.swing.JButton startrunbutton;
    private javax.swing.JSpinner stationcountspinner;
    private javax.swing.JPanel stationedit;
    private javax.swing.JTextField stationnameupdatetxtfield;
    private javax.swing.JButton stationupdateaddbutton;
    private javax.swing.JSpinner stationupdatelsspinner;
    private javax.swing.JComboBox stationupdatepadsizecombobox;
    private javax.swing.JButton stationupdateremovebutton;
    private javax.swing.JButton stationupdateupdatebutton;
    private javax.swing.JTextField systemnameupdatetxtfield;
    private javax.swing.JCheckBox towardscheck;
    private javax.swing.JCheckBox towardscheckbox;
    private javax.swing.JCheckBox tradingrouteplanningcheckbox;
    private javax.swing.JCheckBox uniquecheck;
    private javax.swing.JCheckBox uniquecheckbox;
    private javax.swing.JLabel unladen;
    private javax.swing.JTextField unladentxtfield;
    private javax.swing.JCheckBox updatedataonstartupcheckbox;
    private javax.swing.JCheckBox updatepricebeforeruncheckbox;
    private javax.swing.JCheckBox updatepricescheckbox;
    private javax.swing.JLabel vebosity;
    private javax.swing.JComboBox vebositybox;
    private javax.swing.JPanel vebositypanel;
    private javax.swing.JLabel via;
    private javax.swing.JTextField viaoverridetxtfield;
    private javax.swing.JLabel viatext;
    private javax.swing.JTextField viatxtfield;
    // End of variables declaration//GEN-END:variables
}
