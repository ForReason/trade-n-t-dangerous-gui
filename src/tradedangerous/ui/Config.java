package tradedangerous.ui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author BechtJu
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class Config
{
  
   public Config()
   {
   }

    /**
     *
     * @param key
     * @param filename
     * @return
     * @throws java.io.IOException
     */
    public String[] getConf(String filename, String key[]) throws IOException
   {  
	Properties prop = new Properties();
        FileInputStream fis = null;
        String[] value = key;
        
        
        try{
            // 1. Einstellungen aus einfacher Datei laden
            fis = new FileInputStream(filename);
            prop.load(fis);
            for (int i = 0; i < key.length; i++) {
                value[i] = prop.getProperty(key[i]);
            }
            
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        finally{
            if (fis != null) fis.close();
        }
        return value;
   }
   public void setConf(String filename, String[] key) throws IOException{
    Properties prop = new Properties();
    FileInputStream fis = null;
    FileOutputStream fos = null;
    File file = new File(filename);
    
    try{
        //load old props first (if exists)
       if (file.exists()){
            // 1. Einstellungen aus einfacher Datei laden
            fis = new FileInputStream(filename);
            prop.load(fis);            
        }
        // set properties
        for (int i = 0; i < key.length; i=i+2) {
            prop.setProperty(key[i], key[i+1]);
        }
        
        // save properties
        fos = new FileOutputStream(filename);
        prop.store(fos, "Kommentarzeile");
    }
    catch (IOException ex){
        ex.printStackTrace();
    }
    finally{
        if (fos != null) fos.close();
        if (fis != null) fis.close();
    }
    }
   public void setsingleConf(String filename, String key, String value) throws IOException{
    Properties prop = new Properties();
    FileInputStream fis = null;
    FileOutputStream fos = null;
    File file = new File(filename);
    
    try{
        //load old props first (if exists)
       if (file.exists()){
            // 1. Einstellungen aus einfacher Datei laden
            fis = new FileInputStream(filename);
            prop.load(fis);            
        }
        // set propertie
            prop.setProperty(key, value);
        
        // save properties
        fos = new FileOutputStream(filename);
        prop.store(fos, "Kommentarzeile");
    }
    catch (IOException ex){
        ex.printStackTrace();
    }
    finally{
        if (fos != null) fos.close();
        if (fis != null) fis.close();
    }
    }
   public void deleteconf(String filename){
       File file = new File(filename);
       file.delete();
   }
   public void deletekey(String filename, String key) throws IOException{
       Properties prop = new Properties();
       File file = new File(filename);
       FileInputStream fis = null;
       FileOutputStream fos = null;
       try{
        //load old props first
        fis = new FileInputStream(filename);
        prop.load(fis);
        prop.remove(key);
        // remove key
        
        // save properties
        fos = new FileOutputStream(filename);
        prop.store(fos, "Kommentarzeile");
    }
    catch (IOException ex){
        ex.printStackTrace();
    }
    finally{
        if (fos != null) fos.close();
        if (fis != null) fis.close();
    }
    }
   public String[] getAllvalues(String filename) throws IOException
   {  
	Properties prop = new Properties();
        FileInputStream fis = null;
        fis = new FileInputStream(filename);
        prop.load(fis);
        Set<String> props = prop.stringPropertyNames();
        String[] value  = props.toArray(new String[props.size()]);
        return value;
   }
   public String getsinglevalue(String filename, String key) throws IOException{
        Properties prop = new Properties();
        FileInputStream fis = null;
        String value = key;
        
        
        try{
            // 1. Einstellungen aus einfacher Datei laden
            fis = new FileInputStream(filename);
            prop.load(fis);
                value = prop.getProperty(key);
            }
            
        catch (IOException ex){
            ex.printStackTrace();
        }
        finally{
            if (fis != null) fis.close();
        }
        return value;
   }
   public void settingsmerger(String[] files, String destfile) throws FileNotFoundException, IOException{
       Properties prop = new Properties();
       FileInputStream fis;
       FileOutputStream fos;
       for (String file : files) {
            fis = new FileInputStream(file);
            prop.load(fis);
            fis.close();
       }
       fos = new FileOutputStream(destfile);
       prop.store(fos, "this is a comment");
       fos.close();
   }
   }
    