/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tradedangerous.ui;

import java.io.IOException;

/**
 *
 * @author BechtJu
 */
public class Commandbuilder {
    CmdStarter cmd = new CmdStarter();
    String updatecommando = "trade.py import --plug maddavo";
    
    public void run (Boolean update, String from, int hops, String to, float jdistladen, float jdistunladen, int capacity, String shipsize, float margin, int minstock,
            int mindemand, int mincrperton, int maxcrperton, float maxdataage, String avoid, String via, int prunehops, float prunescore, float lspenalty, int loophops,
            int maxls, int jumps, int maxcargopergood, Boolean unique, Boolean isshort, Boolean isloop, Boolean direct, int credits, String customcode, String vebosity, String debug, int insurance)
            throws IOException, InterruptedException{
        
            String cs = "trade.py run";
                    if(from != null &! from.equals("null") &! from.equals(""))cs = cs+" --from=\""+from+"\"";
                    if(hops != 0)cs = cs+" --hops="+hops;
                    if(to != null &! to.equals("null") &! to.equals("")){cs = cs+" --to=\""+to+"\"";
                    if(isshort == true)cs = cs+" --shorten";}
                    if(credits != 0)cs = cs+" --credits="+credits;
                    if(insurance != 0)cs = cs+" --insurance="+insurance;
                    if(capacity != 0)cs = cs+" --capacity="+capacity;
                    if(jdistladen != 0)cs = cs+" --ly-per="+jdistladen;
                    if(jdistunladen != 0)cs = cs+" --empty-ly="+jdistunladen;
                    if(shipsize != null &! shipsize.equals("null") &! shipsize.equals("") &! shipsize.equals("?"))cs = cs+" --pad-size="+shipsize;
                    if(margin != 0)cs = cs+" --margin="+margin;
                    if(minstock != 0)cs = cs+" --supply="+minstock;
                    if(mindemand != 0)cs = cs+" --demand="+mindemand;
                    if(mincrperton != 0)cs = cs+" --gain-per-ton="+mincrperton;
                    if(maxcrperton != 0)cs = cs+" --max-gain-per-ton="+maxcrperton;
                    if(maxdataage != 0)cs = cs+" --max-days-old="+maxdataage;
                    if(avoid != null &! avoid.equals("null") &! avoid.equals(""))cs = cs+" --avoid="+avoid;
                    if(via != null &! via.equals("null") &! via.equals(""))cs = cs+" --via="+via;
                    if(prunehops != 0)cs = cs+" --prune-hops="+prunehops;
                    if(prunescore != 0)cs = cs+" --prune-score="+prunescore;
                    if(lspenalty != 0)cs = cs+" --ls-penalty="+lspenalty;
                    if(loophops != 0)cs = cs+" --loop-int="+loophops;
                    if(maxls != 0)cs = cs+" --ls-max="+maxls;
                    if(jumps != 0)cs = cs+" --jumps="+jumps;
                    if(maxcargopergood != 0)cs = cs+" --limit="+maxcargopergood;
                    if(customcode != null &! customcode.equals("null") &!customcode.equals(""))cs = cs+" "+customcode;
                    if(unique == true)cs = cs+" --unique";
                    if(isloop == true)cs = cs+" --loop";
                    if(direct == true)cs = cs+" --direct";
                    if(vebosity != null &! vebosity.equals("null") &! vebosity.equals(""))cs = cs+" -"+vebosity;
                    if(debug != null &! debug.equals("null") &! debug.equals(""))cs = cs+" -"+debug;
                    
                   
            
            if (update==true){
                String[] commands = new String[2];
                commands[0] = updatecommando;
                commands[1] = cs;
                cmd.start(commands);
            }else{
                String[] commands = new String[1];
                commands[0] = cs;
                cmd.start(commands);
            }
    }
    public void singlecommand (String Command) throws IOException, InterruptedException{
        cmd.startSingleCommand(Command);
    }
    public static void trade (String[] Commands){
        
    }
    public void buy (Boolean update, int quantity, int supply, String near, int ly, String avoid,
            String pad, Boolean onestop, int maxcredits, Boolean supplysort, Boolean pricessort,Boolean blackmarket, String items) throws IOException, InterruptedException{
        String cs = "trade.py buy";
        if(quantity != 0)cs = cs+" --quantity="+quantity;
        if(supply != 0)cs = cs+" --supply="+supply;
        if(near != null &! near.equals("null") &! near.equals(""))cs = cs+" --near=\""+near+"\"";
        if(ly != 0)cs = cs+" --ly="+quantity;
        if(avoid != null &! avoid.equals("null") &! avoid.equals(""))cs = cs+" --avoid="+avoid;
        if(pad != null &! pad.equals("null") &! pad.equals(""))cs = cs+" --pad-size="+pad;
        if(onestop == true)cs = cs+" --one-stop";
        if(ly != 0)cs = cs+" --lt="+maxcredits;
        if(pricessort == true)cs = cs+" --prices-sort";
        if(supplysort == true)cs = cs+" --supply-sort";
        if(blackmarket == true)cs = cs+" --blackmarket";
        if(items != null &! items.equals("null") &! items.equals(""))cs = cs+" "+items;
        if (update==true){
                String[] commands = new String[2];
                commands[0] = updatecommando;
                commands[1] = cs;
                cmd.start(commands);
            }else{
                String[] commands = new String[1];
                commands[0] = cs;
                cmd.start(commands);
            }
    }
    public static void sell (String[] Commands){
        
    }
    public static void navigate (String[] Commands){
        
    }
    public void stationadd (String system, String station,String padsize,Integer lightseconds) throws IOException, InterruptedException{
        String commando = "trade.py station --add";
               commando = commando + " \"" + system +"/"+station+"\"";
               commando = commando + " --pad=" +padsize;
               commando = commando + " --ls=" +lightseconds.toString();
        cmd.startSingleCommand(commando);
        cmd.startSingleCommand("trade.py export --table Station");
        cmd.startSingleCommand("python -m misc.madupload data/Station.csv");
    }
    public void stationupdate (String system, String station,String padsize,Integer lightseconds) throws IOException, InterruptedException{
        String commando = "trade.py station --update";
               commando = commando + " \"" + system +"/"+station+"\"";
               commando = commando + " --pad=" +padsize;
               commando = commando + " --ls=" +lightseconds.toString();
        cmd.startSingleCommand(commando);
        cmd.startSingleCommand("trade.py export --table Station");
        cmd.startSingleCommand("python -m misc.madupload data/Station.csv");
    }
    public void stationremove (String system, String station) throws IOException, InterruptedException{
        String commando = "trade.py station --remove " +"\""+system+"/"+station+"\"";
        cmd.startSingleCommand(commando);
        cmd.startSingleCommand("trade.py export --table Station");
        cmd.startSingleCommand("python -m misc.madupload data/Station.csv");
    }
}
