/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tradedangerous.ui;

import com.sun.org.apache.xalan.internal.lib.ExsltDatetime;
import java.io.*;
import javax.swing.JOptionPane;

/**
 *
 * @author BechtJu
 */
public class CmdStarter {
    


public void start(String[] todo) throws IOException, InterruptedException {
    Config conf = new Config();
    Process p;
    p = Runtime.getRuntime().exec("cmd"); //Start CMD
    new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
    new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
    PrintWriter stdin = new PrintWriter(p.getOutputStream());
    
    stdin.println("cd " + conf.getsinglevalue("overall","path"));//goto trade path in cmd
/*########## Comandshredder now ##########*/
    for (int i = 0; i < todo.length; i++) { //for i as long as Array "Todo/Commands" got entries
    stdin.println(todo[i]); //type next command of Todo array into cmd    
    }
/*########## Commandshredder finnished here ##########*/
    stdin.println("exit"); //exit commantline so you dont have a waste of cmd processes
    stdin.close();
    
/* Debug
    int returnCode = p.waitFor();
    System.out.println("Return code = " + returnCode);
//finish debug */      
  }
public void startSingleCommand(String todo) throws IOException, InterruptedException {
    Config conf = new Config();
    Process p;
    p = Runtime.getRuntime().exec("cmd"); //Start CMD
    new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
    new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
    PrintWriter stdin = new PrintWriter(p.getOutputStream());
    
    stdin.println("cd " + conf.getsinglevalue("overall","path"));//goto trade path in cmd
    stdin.println(todo); //type next command into cmd    
    stdin.println("exit"); //exit commantline so you dont have a waste of cmd processes
    stdin.close();
    
/* Debug
    int returnCode = p.waitFor();
    System.out.println("Return code = " + returnCode);
//finish debug */      
  }
}
    

